<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
             array(
                
                0=>array(
                    'id'=>804040,
                    'category_name'=>"Sonstige Umzugsleistungen",
                    'category_img'=>"https://d6qjsjk30xa1n.cloudfront.net/bundles/myhammerjob/images/services/1_25.svg?v=1535369703",
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()
                ),
                1=>array(
                    'id'=>802030,
                    'category_name'=>"Abtransport, Entsorgung und Entrümpelung",
                    'category_img'=>"https://d6qjsjk30xa1n.cloudfront.net/bundles/myhammerjob/images/services/1_29.svg?v=1535369703",
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()

                ),

                2=>array(
                    'id'=>411070,
                    'category_name'=>"Fensterreinigung",
                    'category_img'=>"https://d6qjsjk30xa1n.cloudfront.net/bundles/myhammerjob/images/services/1_28.svg?v=1535369703",
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()
                ),
                3=>array(
                    'id'=>402020,
                    'category_name'=>"Holzdielen schleifen",
                    'category_img'=>"https://d6qjsjk30xa1n.cloudfront.net/bundles/myhammerjob/images/services/1_11.svg?v=1535369703",
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()

                ),
                4=>array(
                    'id'=>108140,
                    'category_name'=>"Kellersanierung",
                    'category_img'=>"https://d6qjsjk30xa1n.cloudfront.net/bundles/myhammerjob/images/services/1_22.svg?v=1535369703",
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()

                )

             )
        );
    }
}
