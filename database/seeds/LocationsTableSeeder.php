<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->insert(
             array(
                
                0=>array(
                    'zipcode'=>"10115",
                    'city'=>"Berlin",
                    'country'=>"Germany",
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()
                ),
                1=>array(
                    'zipcode'=>"32457",
                    'city'=>"Porta Westfalica",
                     'country'=>"Germany",
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()

                ),

                2=>array(
                    'zipcode'=>"01623",
                    'city'=>"Lommatzsch",
                    'country'=>"Germany",
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()
                ),
                3=>array(
                    'zipcode'=>"21521",
                    'city'=>"Hamburg",
                    'country'=>"Germany",
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()

                ),
                4=>array(
                    'zipcode'=>"06895",
                    'city'=>"Bülzig",
                    'country'=>"Germany",
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()

                ),
              5=>array(
                    'zipcode'=>"01612",
                    'city'=>"Diesbar-Seußlitz",
                    'country'=>"Germany",
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()

                )
            )
        );
    }
}
