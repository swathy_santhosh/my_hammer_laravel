<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LocationTest extends TestCase
{
    /**
     * Check if the query string matches with the German City.
     *
     * @return void
     */
    public function testLocationByZipCode()
    {
        $response = $this->json("GET", "/list/locations/101");
       
      

        $response->assertStatus(200)
                ->assertSee("Germany")
                ->assertSee("Berlin")
                ->assertSee("101")
                ->assertDontSee("Amsterdam");
    }

    public function testLocationByZipCodeReturnsEmpty()
    {
        $response = $this->json("GET", "/list/locations/123");

        $response
            ->assertStatus(200)
            ->assertJson([
                "data"=>[]
                
                ]);
    }
}
