<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
     
     // Test List all method

    public function testlistAllCategories()
    {
        $response = $this->json("GET", "/category");
       
      

        $response->assertStatus(200)
                 ->assertSee("Kellersanierung")
                 ->assertSee("Fensterreinigung")
                 ->assertSee("108140")
                 ->assertSee("411070")
                 ->assertJsonFragment([
                    'category_name' => 'Kellersanierung',
                    'id'=>108140
                    
                ]);
    }


    public function testlistAllCategoriesFails()
    {
        $response = $this->json("GET", "/category");
       
      

        $response->assertStatus(200)
                 ->assertDontSee("Innen")
                 ->assertDontSee("Metall")
                 ->assertDontSee("234531")
                 ->assertDontSee("454235")
                 ->assertJsonMissing([
                    'category_name' => 'Sonstiges',
                    'id'=>354343
                    
                ]);
    }
}
