<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testOrderPostPasses()
    {
        $response =$this->json('POST', '/orders', [
                    "title"=>"Need to paint my garage",
                    "zipcode"=>"01623",
                    "description"=>"Garage paint has completely gone",
                    "execution_date"=>"29-08-2018",
                    "category_id"=>"402020"
                 ]);
        
        $response
            ->assertStatus(200)
            ->assertJson([
                 "data"=>"Successfully submitted your order"
            ]);
    }

    public function testOrderPostFailTestForTitle()
    {
        $response =$this->json('POST', '/orders', [
                    "title"=>" ",
                    "zipcode"=>"01623",
                    "description"=>"Garage paint has completely gone",
                    "execution_date"=>"29-08-2018",
                    "category_id"=>"40"
                 ]);
        
        $response
            ->assertStatus(422)
           ->assertJsonValidationErrors("title")
           ->assertSee("The title must be a string.")
           ->assertSee("The title field is required.");
    }
}
