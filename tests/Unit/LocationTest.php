<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Services\LocationService;

class LocationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
     
     

     

    public function testFindByZipCodeReturnsObject()
    {
        // Create a stub for the LocationService class.
        $stub = $this->createMock(LocationService::class);
        
        // Configure the stub.
        $stub->method('findByZipCode')->will($this->returnSelf());

        // $stub->findByCity('101') returns '10115'
        $this->assertEquals($stub, $stub->findByZipCode('101'));
    }
}
