This application uses PHP 7.1 with Laravel 5.6 framework. It uses MYSQL as a database.

Note: Make sure that composer installed. If not, install composer and proceed it.
Kindly follow the below steps to run this application.

1 Clone this application using the below command in your local machine.
     git clone https://swathy_santhosh@bitbucket.org/swathy_santhosh/my_hammer_laravel.git

2 Navigate to the directory in terminal where this source code is cloned. 

3 Run "composer install" .This will install the dependencies that is required for the application to run.

4 Open the source folder in any IDE (such as Visual studio code,sublime etc) and create .env (copy           .env.dist)

5 Configure .env file by providing the required value such as db credentials.Before testing database    connectivity, create database for this application (eg. swathy_hammer_prj_laravel) 

6 To run database migrations :  
        php artisan migrate

        check if the tables are created in the database (category, orders,location)
7 To dataseed the table :
        php artisan db:seed --CategoriesTableSeeder

        php artisan db:seed --LocationsTableSeeder 

        check if the category and location tables are loaded with initial data that is required to demonstrate this coding challenge.

8 To run the application :
      php artisan serve
    This command will start a development server at http://localhost:8000

9 The list of routes are available in the below url
    https://documenter.getpostman.com/view/1840561/RWTsrFgU
         
10 To run test :
    ./vendor/bin/phpunit
