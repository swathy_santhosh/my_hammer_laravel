<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function orders()
    {
        return $this->hasMany("App\Order");
    }

    protected $table ='categories';
}
