<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public function orders()
    {
        return $this->hasMany("App\Order");
    }

    protected $table= 'locations' ;
}
