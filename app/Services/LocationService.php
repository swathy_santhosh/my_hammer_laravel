<?php

namespace App\Services;

use App\Models\Location;
use App\Repositories\LocationRepositoryInterface;

class LocationService implements LocationRepositoryInterface
{

    // Find By query string

    public function findByZipCode($zipcode):Object
    {
        return Location::where("zipcode", "like", '%'.$zipcode.'%')->get();
    }
}
