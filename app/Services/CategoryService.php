<?php

namespace App\Services;

use App\Models\Category;
use App\Repositories\CategoryRepositoryInterface;

class CategoryService implements CategoryRepositoryInterface
{

    // List All Categories

    public function listAllCategories():Object
    {
        return  Category::all();
    }
}
