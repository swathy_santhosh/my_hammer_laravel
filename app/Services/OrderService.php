<?php

namespace App\Services;

use App\Repositories\OrderRepositoryInterface; 
use App\Models\{Location,User,Category,Order}; 
use Illuminate\Support\Facades\Hash;

class OrderService implements OrderRepositoryInterface
{
    public function submit_order($request):Object
    {
        $order = new Order;
        $order->title = $request->input("title");
        $order->zipcode = $request->input("zipcode");
        $order->description = $request->input("description");
        $order->execution_date =date('Y-m-d', strtotime($request->input("execution_date")));
        // $order->user_id = $id; in real time, the value will be passed from the client
        $order->user_id = 1; // for demonstration purpose
        $order->category_id = $request->input("category_id");
        $order->save();
        return $order;
    }
}
