<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'title'=>'String|required|min:5|max:50',
             'zipcode'=>'required|min:5|exists:locations,zipcode',
             'description'=>'required|String|max:1000',
             'execution_date'=>'required|date',
             'category_id'=>'required|integer|exists:categories,id'
        ];
    }
}
