<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use App\Models\Category; 
use App\Repositories\CategoryRepositoryInterface;

class CategoryController extends Controller
{
    protected $category;

    protected $request;
     /**
     * Create a new controller instance.
     *
     * @param  CategoryRepositoryInterface  $category
     * @return void
     */

    public function __construct(Request $request,Response $response,CategoryRepositoryInterface $category)
    {
          $this->category = $category;         
          $this->request = $request;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $categories = $this->category->listAllCategories();
         return response()->json(["data"=>$categories]);
    }
}
